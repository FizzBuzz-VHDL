-------------------------------------------------------------------------------
-- Test-bench to validate the FizzBuzz output
-- Copyright 2020 Christophe CURIS
-------------------------------------------------------------------------------
--
--  This file is part of FizzBuzz-VHDL.
--
--  FizzBuzz-VHDL is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation, version 3.
--
--  FizzBuzz-VHDL is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with FizzBuzz-VHDL.  If not, see <https://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
-- This bench runs the component and print its output on STDOUT.
--
-- Users should be aware that there is a small limitation to this test-bench.
-- Very small. A trivial point of detail. The test-bench actually does not
-- check that the output is correct. But who cares nowadays...
-- The reason for this is that... well... nobody cared to implement a binding
-- to the TensorFlow library in VHDL ?
-------------------------------------------------------------------------------
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity bench is
	generic (
		line_length : integer := 80;
		stop_value  : integer := 100
	);
end entity;


architecture TB of bench is

	signal rst_n : std_logic;
	signal clk   : std_logic;

	constant clk_period : time := 5 ns;  -- 200 MHz

	signal valid : std_logic;
	signal data  : std_logic_vector(7 downto 0);

	function binary_to_character(bin : std_logic_vector) return character is
	begin
		return character'val(to_integer(unsigned(bin)));
	end function binary_to_character;

	type operating_state_t is (AWAITING_FOR_THE_MIRACLE, HEAVY_DUTY_OCCURING, ALMOST_THERE, AT_LEAST_VACATION, A_FLY_IN_THE_OINTMENT);
	signal whats_happening : operating_state_t;

begin

	----------------------------------------------------------------------------

	DUT: entity work.FizzBuzz
		generic map (
			stop_value  =>  stop_value
		)
		port map (
			rst_n => rst_n,
			clk   => clk,
			valid => valid,
			data  => data
		);

	----------------------------------------------------------------------------

	rst_n <= '0', '1' after 100 ns;

	gen_clock: process is
	begin
		while whats_happening /= AT_LEAST_VACATION loop
			clk <= '0';
			wait for clk_period / 2;
			clk <= '1';
			wait for clk_period / 2;
		end loop;
		clk <= '0';
		wait for clk_period / 2;
		-- At this point simulation will stop because there are no more pending event
		wait;
	end process gen_clock;

	----------------------------------------------------------------------------

	whats_happening <= AWAITING_FOR_THE_MIRACLE when rst_n = '0'         else
	                   A_FLY_IN_THE_OINTMENT    when to_X01(valid) = 'X' else
	                   HEAVY_DUTY_OCCURING      when valid = '1'         else
	                   ALMOST_THERE, AT_LEAST_VACATION after clk_period when falling_edge(valid);

	----------------------------------------------------------------------------

	print_output: process (rst_n, clk) is
		variable line_nb       : integer := 1;
		variable captured_data : string(1 to line_length + 16);
		variable idx_wr        : integer;
		variable idx_print_to  : integer;

		procedure print_line(print_to : integer) is
			variable wr_line : line;
		begin
			write(wr_line, string'("["));
			write(wr_line, line_nb, field => 3, justified => right);
			write(wr_line, "] = " & captured_data(1 to print_to));
			writeline(OUTPUT, wr_line);
			line_nb := line_nb + 1;
		end procedure;

	begin
		if rst_n = '0' then
			idx_wr := captured_data'left;
		elsif falling_edge(clk) then
			if valid = '1' then
				captured_data(idx_wr) := binary_to_character(data);
				if idx_wr > line_length - 8 then

					-- Break lines on a space, do not break text/numbers
					for i in idx_wr downto captured_data'left loop
						idx_print_to := i;
						exit when captured_data(i) = ' ';
					end loop;

					-- Print accumulated characters so far
					print_line(idx_print_to - 1);

					-- Put unprinted characters at beginning of next line
					captured_data(1 to idx_wr - idx_print_to) := captured_data(idx_print_to + 1 to idx_wr);
					idx_wr := (idx_wr - idx_print_to) + 1;
				else
					idx_wr := idx_wr + 1;
				end if;
			elsif idx_wr > captured_data'left then
				-- This means we still have unprinted data, flush them
				print_line(idx_wr - 1);
				idx_wr := captured_data'left;
			end if;
		end if;
	end process print_output;

	----------------------------------------------------------------------------

	measure_bpn: process (clk)
		variable count_cycles : integer := 0;
		variable wr_line : line;
	begin
		if falling_edge(clk) then
			if valid = '1' then
				count_cycles := count_cycles + 1;
			elsif whats_happening = AT_LEAST_VACATION then
				wr_line := new string'("Info: Average byte per number = ");
				write(wr_line, real(count_cycles) / real(stop_value));
				write(wr_line, " for " & integer'image(stop_value) & " numbers");
				writeline(OUTPUT, wr_line);
			end if;
		end if;
	end process measure_bpn;

	----------------------------------------------------------------------------

end architecture TB;
