SOURCES = src/fizzbuzz.vhdl

BENCH_SRC = bench_src/bench.vhdl

COMPILE = ghdl -a
   LINK = ghdl -e

############################################################

OBJECTS = $(SOURCES:src/%.vhdl=%.o)

all:: simulation.ghw

clean:
	rm -f $(OBJECTS) bench.o bench simulation.ghw work-obj93.cf e~bench.o

1% 2% 3% 4% 5% 6% 7% 8% 9%: bench
	./$< -gstop_value=$@ -gline_length=$(shell tput cols)

############################################################

%.o: src/%.vhdl
	$(COMPILE) $(SOURCES)

bench: $(OBJECTS) $(BENCH_SRC)
	$(COMPILE) $(BENCH_SRC)
	$(LINK) $@

simulation.ghw:: bench
	./$< --wave=$@
