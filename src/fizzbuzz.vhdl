-------------------------------------------------------------------------------
-- Synthesisable FizzBuzz Generator
-- Copyright 2020 Christophe CURIS
-------------------------------------------------------------------------------
--
--  This file is part of FizzBuzz-VHDL.
--
--  FizzBuzz-VHDL is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Lesser General Public License as published
--  by the Free Software Foundation, version 3.
--
--  FizzBuzz-VHDL is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Lesser General Public License for more details.
--
--  You should have received a copy of the GNU Lesser General Public License
--  along with FizzBuzz-VHDL.  If not, see <https://www.gnu.org/licenses/>.
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


entity FizzBuzz is
	generic (
		stop_value  : integer := 100
	);
	port (
		rst_n :  in std_logic;
		clk   :  in std_logic;

		valid : out std_logic;
		data  : out std_logic_vector(7 downto 0)
	);
end entity FizzBuzz;


architecture RTL of FizzBuzz is

	----------------------------------------------------------------------------
	-- The data types we will need
	----------------------------------------------------------------------------

	-- Our counter is working to BCD because it makes data-out logic a lot simpler
	subtype bcd_digit_t  is unsigned(3 downto 0);
	type    bcd_number_t is array (natural range <>) of bcd_digit_t;

	-- Storage for text strings
	type unsigned_vector is array (natural range<>) of unsigned(data'range);

	----------------------------------------------------------------------------
	-- Helper functions
	----------------------------------------------------------------------------

	-- Because it does not exist in the language
	function max (a, b: integer) return integer is
	begin
		if a > b then return a; else return b; end if;
	end function max;

	-- Generate an ASCII string in "unsigned" from a vhdl string
	function uvstring(constant str: string) return unsigned_vector is
		variable vec : unsigned_vector(str'range);
	begin
		for i in str'range loop
			vec(i) := to_unsigned(character'pos(str(i)), vec(i)'length);
		end loop;
		return vec;
	end function uvstring;

	-- Convert an integer value into a BCD typed value
	function integer_to_bcd(constant int: integer; constant size: integer) return bcd_number_t is
		variable result : bcd_number_t(size downto 1);
		variable value  : integer;
	begin
		value := int;
		for i in result'reverse_range loop
			result(i) := to_unsigned(value mod 10, bcd_digit_t'length);
			value := value / 10;
		end loop;
		return result;
	end function integer_to_bcd;

	----------------------------------------------------------------------------

	constant nb_digit : integer := integer(floor(log10(real(stop_value)))) + 1;

	signal counter : bcd_number_t(nb_digit downto 1);

	constant last_value : bcd_number_t(counter'range) := integer_to_bcd(stop_value, counter'length);

	signal please_wait : std_logic;

	signal is_fizz : std_logic;
	signal is_buzz : std_logic;

	----------------------------------------------------------------------------
	-- Storage for the special strings
	----------------------------------------------------------------------------

	constant fizzbuzz_string : unsigned_vector(8 downto 1) := uvstring("FizzBuzz");
	alias    fizz_string     : unsigned_vector(4 downto 1) is fizzbuzz_string(8 downto 5);
	alias    buzz_string     : unsigned_vector(4 downto 1) is fizzbuzz_string(4 downto 1);

	signal   used_digits     : integer range 1 to max(nb_digit, fizzbuzz_string'length);

begin

	----------------------------------------------------------------------------

	counter_to_stop_value: process (clk, rst_n) is
		variable increment_next_digit : boolean;
	begin
		if rst_n = '0' then
			counter <= (others => "0000");
		elsif rising_edge(clk) then
			increment_next_digit := (please_wait = '0');
			for i in counter'reverse_range loop
				if increment_next_digit then
					if counter(i) = "1001" then
						increment_next_digit := true;
						counter(i) <= "0000";
					else
						increment_next_digit := false;
						counter(i) <= counter(i) + 1;
					end if;
				end if;
			end loop;
		end if;
	end process counter_to_stop_value;

	----------------------------------------------------------------------------

	work_on_number: block is
		signal count_3 : unsigned(1 downto 0);
		signal count_5 : unsigned(2 downto 0);
	begin

		is_fizzbuzz: process (rst_n, clk) is
		begin
			if rst_n = '0' then
				count_3 <= (others => '0');
				is_fizz <= '0';
				count_5 <= (others => '0');
				is_buzz <= '0';
			elsif rising_edge(clk) then
				if please_wait = '0' then
					if count_3 = 2 then
						count_3 <= (others => '0');
						is_fizz <= '1';
					else
						count_3 <= count_3 + 1;
						is_fizz <= '0';
					end if;

					if count_5 = 4 then
						count_5 <= (others => '0');
						is_buzz <= '1';
					else
						count_5 <= count_5 + 1;
						is_buzz <= '0';
					end if;
				end if;
			end if;
		end process is_fizzbuzz;

	end block work_on_number;

	----------------------------------------------------------------------------

	count_digits: process (is_fizz, is_buzz, counter) is
	begin
		if is_fizz = '1' and is_buzz = '1' then
			used_digits <= 8;
		elsif is_fizz = '1' or is_buzz = '1' then
			used_digits <= 4;
		else
			for i in counter'range loop
				used_digits <= i;
				exit when counter(i) /= "0000";
			end loop;
		end if;
	end process count_digits;

	----------------------------------------------------------------------------

	generate_output: block is
		type state_t is (CAPTURE_NUMBER, OUTPUT_DIGITS, OUTPUT_SEPARATOR, SEQUENCE_FINISHED);
		signal state : state_t;

		constant base_digit      : unsigned(data'range) := to_unsigned(character'pos('0'), data'length);
		constant separator_char  : unsigned(data'range) := to_unsigned(character'pos(','), data'length);
		constant end_of_stream   : unsigned(data'range) := to_unsigned(character'pos(EOT), data'length);
		constant space_character : unsigned(data'range) := to_unsigned(character'pos(' '), data'length);

		signal current_digit : integer range 1 to max(nb_digit, fizzbuzz_string'length);

		signal data_u : unsigned(data'range);
	begin

		state_machine: process (rst_n, clk)
		begin
			if rst_n = '0' then
				current_digit <= 1;   -- Arbitrary, to avoid uninitialized registers
				state <= CAPTURE_NUMBER;
				please_wait <= '0';
			elsif rising_edge(clk) then
				case state is
					when CAPTURE_NUMBER =>
						state <= OUTPUT_DIGITS;
						current_digit <= used_digits;
						please_wait <= '1';

					when OUTPUT_DIGITS =>
						if current_digit > 1 then
							current_digit <= current_digit - 1;
							state <= OUTPUT_DIGITS;
							please_wait <= '1';
						elsif counter = last_value then
							state <= SEQUENCE_FINISHED;
							please_wait <= '1';
						else
							state <= OUTPUT_SEPARATOR;
							-- Update counter now so that we have 1 cycle to calculate
							-- the 'used_digits/is_fizz/is_buzz'
							please_wait <= '0';
						end if;

					when OUTPUT_SEPARATOR =>
						state <= CAPTURE_NUMBER;
						please_wait <= '1';

					when SEQUENCE_FINISHED =>
						-- Loop forever in this state
						state <= SEQUENCE_FINISHED;
						please_wait <= '1';

				end case;
			end if;
		end process state_machine;

		data_u <= space_character                when state = CAPTURE_NUMBER          else
		          separator_char                 when state = OUTPUT_SEPARATOR        else
		          end_of_stream                  when state = SEQUENCE_FINISHED       else
		          fizzbuzz_string(current_digit) when is_buzz = '1' and is_fizz = '1' else
		          fizz_string(current_digit)     when is_fizz = '1'                   else
		          buzz_string(current_digit)     when is_buzz = '1'                   else
		          base_digit(7 downto 4) & counter(current_digit);

		sync_output: process (rst_n, clk)
		begin
			if rst_n = '0' then
				data <= (others => '0');
				valid <= '0';
			elsif rising_edge(clk) then
				data <= std_logic_vector(data_u);
				if state = SEQUENCE_FINISHED then
					valid <= '0';
				elsif state = OUTPUT_DIGITS then
					valid <= '1';
				end if;
			end if;
		end process sync_output;

	end block generate_output;

	----------------------------------------------------------------------------

end architecture RTL;
